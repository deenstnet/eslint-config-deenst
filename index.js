module.exports = {
  'env': {
      'commonjs': true,
      'es6': true,
      'node': true
  },
  'extends': 'eslint:recommended',
  'globals': {
      'Atomics': 'readonly',
      'SharedArrayBuffer': 'readonly'
  },
  'parserOptions': {
      'ecmaVersion': 2018
  },
  'rules': {
      'require-atomic-updates': "off",
      'no-unused-vars': ["error", { "args": "none" }],
      'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
      'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
      'indent': [
          'error',
          2
      ],
      'linebreak-style': [
          'error',
          'unix'
      ],
      'quotes': [
          'error',
          'single'
      ],
      'semi': [
          'error',
          'always'
      ],
      'no-process-env': [
          'error'
      ],
      'brace-style': [
          'error',
          '1tbs'
      ]
  },
  'overrides': [
      {
          'files': [
              'config.js'
          ],
          'rules': {
              'no-process-env': 'off'
          }
      }
  ]
};